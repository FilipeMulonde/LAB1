#ifndef CIRCLE_H
#define CIRCLE_H
#include "Shape.hpp"
#include "base-types.hpp"

class Circle :public Shape {
public:
	Circle(double centerX, double centerY, double radius);
	double getArea() const;
	rectangle_t getFrame() const;
	void move(double dx, double dy);
	void move(point_t p);
	void Show() const;

private:
	point_t center_;
	double radius_;
};

#endif
