#include "rectangle.hpp"
#include <iostream>

Rectangle::Rectangle(double centerX, double centerY, double width, double height) :rectangle_({ centerX, centerY, width, height }) {}
double Rectangle::getArea() const
{
	return rectangle_.width * rectangle_.height;
}
rectangle_t Rectangle::getFrame() const
{
	return rectangle_;
}
void Rectangle::move(double dx, double dy)
{
	rectangle_.pos.x += dx;
	rectangle_.pos.y += dy;
}
void Rectangle::move(point_t p)
{
	rectangle_.pos = p;
}
void Rectangle::show() const
{
	std::cout << "Area of a Rectangle: " << Rectangle::getArea() << std::endl;
	std::cout << "Frame X : " << rectangle_.pos.x << " Frame Y: " << rectangle_.pos.y << std::endl;
	std::cout << "Frame -Width: " << rectangle_.width << ";Frame Height: " << rectangle_.height << "\n\n";
}
