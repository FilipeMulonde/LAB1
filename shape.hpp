#ifndef SHAPE_H
#define SHAPE_H
#include "base-types.hpp"

class Shape
{
public:
	virtual double getArea() const = 0;
	virtual rectangle_t getFrame() const = 0;
	virtual void move(double dx, double dy) = 0;
	virtual void move(point_t p) = 0;
  virtual void show() const = 0;
};

#endif
