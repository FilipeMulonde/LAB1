#include "Triangle.hpp"
#include "base-types.hpp"
#include <algorithm>
#include <iostream>

Triangle::Triangle(point_t p1, point_t p2, point_t p3) :a_(p1), b_(p2), c_(p3) {}

double Triangle::getArea() const
{
	double ab = FindDistance(a_, b_);
	double ac = FindDistance(a_, c_);
	double bc = FindDistance(b_, c_);
	double s = (ab + ac + bc) / 2;
	return  sqrt(s*(s - ab)*(s - ac)*(s - bc));
}
rectangle_t Triangle::getFrame() const
{
	using namespace std;
	double left   =  min(min(a_.x, b_.x),  c_.x);
	double bottom = min(min(a_.y, b_.y),   c_.y);
	double height = abs(max(max(a_.y, b_.y), c_.y) - bottom);
	double width =  abs(max(max(a_.x, b_.x), c_.x) - left);
	return rectangle_t{ point_t{left + width / 2, bottom + height / 2}, width, height};
}


void Triangle::move(double dx, double dy)
{
	a_.x += dx;
	a_.y += dy;
	b_.x += dx;
	b_.y += dy;
	c_.x += dx;
	c_.y += dy;
}
void Triangle::move(point_t p)
{
	move(p.x - FindCenter().x, p.y - FindCenter().y);
}
point_t Triangle::FindCenter() const
{
	return point_t{ (a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3 };
}
double Triangle::FindDistance(point_t p1, point_t p2) const
{
	return sqrt((p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y));
}

void Triangle::Show() const
{
	std::cout << "Area of a Triangle: " << Triangle::getArea() << std::endl;
  point_t Center_ = Triangle::FindCenter();
	std::cout << "Center of a Triangle: " << " X: " << Center_.x << " Y: " << Center_.y << std::endl;
	rectangle_t frame = Triangle::getFrame();
	std::cout << "Frame X : " << frame.pos.x << ";Frame Y: " << frame.pos.y << std::endl;
	std::cout << "Frame -Width: " << frame.width << ";Frame Height: " << frame.height << "\n\n";
}
